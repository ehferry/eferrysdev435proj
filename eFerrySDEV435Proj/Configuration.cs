﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file which contains all methods used to update the App.config file connected with this application
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace eFerrySDEV435Proj
{
    class Configuration
    {
        /*
        * Method Name: getConfig
        * Description: Checks the App.config file for the current settings
        */
        public string getConfig(string _key)
        {
            // Grab current value for the setting/key
            string value = ConfigurationManager.AppSettings[_key];
            return value;
        }

        /*
        * Method Name: setConfig
        * Description: Sets the App.config file record for the current settings
        */
        public void setConfig(string _key, string _value)
        {
            // Open to write to App.config
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            // Set new value for the setting
            config.AppSettings.Settings[_key].Value = _value;

            // Save our setting change
            config.Save(ConfigurationSaveMode.Modified);

            // Reload App.config appSettings section
            ConfigurationManager.RefreshSection("appSettings");
        }

    }
}
