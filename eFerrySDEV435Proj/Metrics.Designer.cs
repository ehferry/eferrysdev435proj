﻿namespace eFerrySDEV435Proj
{
    partial class Metrics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Metrics));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxLowIncomeYN = new System.Windows.Forms.CheckBox();
            this.checkBoxUnsecureYN = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCSMin = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxHighBal = new System.Windows.Forms.TextBox();
            this.textBoxLowBal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonProcess = new System.Windows.Forms.Button();
            this.buttonCancelClose = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "App Metric Configuration";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxLowIncomeYN);
            this.panel1.Controls.Add(this.checkBoxUnsecureYN);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBoxCSMin);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxHighBal);
            this.panel1.Controls.Add(this.textBoxLowBal);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(17, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(444, 375);
            this.panel1.TabIndex = 2;
            // 
            // checkBoxLowIncomeYN
            // 
            this.checkBoxLowIncomeYN.AutoSize = true;
            this.checkBoxLowIncomeYN.Location = new System.Drawing.Point(182, 295);
            this.checkBoxLowIncomeYN.Name = "checkBoxLowIncomeYN";
            this.checkBoxLowIncomeYN.Size = new System.Drawing.Size(15, 14);
            this.checkBoxLowIncomeYN.TabIndex = 13;
            this.checkBoxLowIncomeYN.UseVisualStyleBackColor = true;
            // 
            // checkBoxUnsecureYN
            // 
            this.checkBoxUnsecureYN.AutoSize = true;
            this.checkBoxUnsecureYN.Location = new System.Drawing.Point(182, 236);
            this.checkBoxUnsecureYN.Name = "checkBoxUnsecureYN";
            this.checkBoxUnsecureYN.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUnsecureYN.TabIndex = 12;
            this.checkBoxUnsecureYN.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(13, 327);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(358, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Checking this will remove risk rating calculations for the Borrower category.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 295);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Low Income Lending";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Allow Unsecured Loans";
            // 
            // textBoxCSMin
            // 
            this.textBoxCSMin.Location = new System.Drawing.Point(166, 159);
            this.textBoxCSMin.MaxLength = 3;
            this.textBoxCSMin.Name = "textBoxCSMin";
            this.textBoxCSMin.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxCSMin.Size = new System.Drawing.Size(100, 20);
            this.textBoxCSMin.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Credit Score Minimum";
            // 
            // textBoxHighBal
            // 
            this.textBoxHighBal.Location = new System.Drawing.Point(137, 85);
            this.textBoxHighBal.MaxLength = 20;
            this.textBoxHighBal.Name = "textBoxHighBal";
            this.textBoxHighBal.Size = new System.Drawing.Size(100, 20);
            this.textBoxHighBal.TabIndex = 6;
            // 
            // textBoxLowBal
            // 
            this.textBoxLowBal.Location = new System.Drawing.Point(137, 49);
            this.textBoxLowBal.MaxLength = 20;
            this.textBoxLowBal.Name = "textBoxLowBal";
            this.textBoxLowBal.Size = new System.Drawing.Size(100, 20);
            this.textBoxLowBal.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Highest Balance";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Lowest Balance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Loan Amount Limit";
            // 
            // buttonProcess
            // 
            this.buttonProcess.Location = new System.Drawing.Point(386, 437);
            this.buttonProcess.Name = "buttonProcess";
            this.buttonProcess.Size = new System.Drawing.Size(75, 23);
            this.buttonProcess.TabIndex = 3;
            this.buttonProcess.Text = "Process";
            this.buttonProcess.UseVisualStyleBackColor = true;
            this.buttonProcess.Click += new System.EventHandler(this.buttonProcess_Click);
            // 
            // buttonCancelClose
            // 
            this.buttonCancelClose.Location = new System.Drawing.Point(305, 437);
            this.buttonCancelClose.Name = "buttonCancelClose";
            this.buttonCancelClose.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelClose.TabIndex = 4;
            this.buttonCancelClose.Text = "Close";
            this.buttonCancelClose.UseVisualStyleBackColor = true;
            this.buttonCancelClose.Click += new System.EventHandler(this.buttonCancelClose_Click);
            // 
            // Metrics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 469);
            this.Controls.Add(this.buttonCancelClose);
            this.Controls.Add(this.buttonProcess);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Metrics";
            this.Text = "Metrics";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonProcess;
        private System.Windows.Forms.Button buttonCancelClose;
        private System.Windows.Forms.TextBox textBoxHighBal;
        private System.Windows.Forms.TextBox textBoxLowBal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCSMin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBoxLowIncomeYN;
        private System.Windows.Forms.CheckBox checkBoxUnsecureYN;
        private System.Windows.Forms.Label label8;
    }
}