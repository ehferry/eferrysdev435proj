﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file containing the validation class used in the loan risk calculator app.  
 *
 */

// Includes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace eFerrySDEV435Proj
{
    /*
    * Class Name: Validate
    * Description: Contains all methods that validate user inputs
    */
    class Validate
    {
        /*
        * Method Name: isInteger
        * Description: Returns true if the parameter is an integer
        */
        public bool isInteger(string input)
        {
            // Use TryParse to determine if string can be converted to int
            if (int.TryParse(input, out int x))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /*
        * Method Name: isDouble
        * Description: Returns true if the parameter is a double
        */
        public bool isDouble(string input)
        {
            // Use TryParse to determine if string can be converted to double
            if (double.TryParse(input, out double x))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /*
        * Method Name: isIntRate
        * Description: Returns true if the parameter is a valid interest rate
        */
        public bool isIntRate(string input)
        {
            // Check if interest rate string matches XX.XX format through regex
            if (Regex.IsMatch(input, @"(([0-9])([0-9]).([0-9])([0-9]))\b"))
            {

            }
            else
            {
                return false;
            }

            double.TryParse(input, out double x);

            // These values are the recognized range of possible interest rates; 8.0 - 35.0
            if (x < 8.0)
            {
                return false;
            }
            else if (x > 35.0)
            {
                return false;
            }

            return true;
        }

        /*
         * Method Name: isCreditScore
         * Description: Returns true if the parameter is a valid credit score; within 850 - 300 and is greater than any existing minimum
         */
        public bool isCreditScore(int input, int creditMin)
        {
            if (input > 850)
            {
                return false;
            }
            else if (input < 300)
            {
                return false;
            }
            else if (creditMin != 0)
            {
                if (input < creditMin)
                {
                    return false;
                }
            }

            // If the score is within the expected range and not less than any minimum then return true
            return true;
        }

        /*
         * Method Name: greaterThan
         * Description: Returns true if first vaule is greater than the second value
         */
        public bool greaterThan(int first, int second)
        {
            if (first == second)
            {
                return false;
            }
            else if (first < second)
            {
                return false;
            }

            return true;
        }

        /*
         * Method Name: lienRestrict
         * Description: Returns true if partial lien is less that 75% of the full loan balance
         */
        public bool lienRestrict(int fullLoan, int partLien)
        {
            double perc = partLien / fullLoan * 100;

            if (perc >= 75)
            {
                return false;
            }

            return true;
        }

    }
}
