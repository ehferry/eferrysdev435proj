﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file connected to the main screen of the loan risk calculator app. 
 * It contains three buttons that the user can use to navigate or exit the app. 
 */

//Includes
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Project Namespace
namespace eFerrySDEV435Proj
{
    // Form Class in Use
    public partial class MainForm : Form
    {
        // Code to run on form creation
        public MainForm()
        {           
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Any styling code goes here
        }

        // Code to run on Calculate click
        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            //This will open the calculate form and close the main form
            this.Hide();
            Calculator calculator = new Calculator();
            calculator.Closed += (s, args) => this.Close();
            calculator.Show();
        }

        // Code to run on Metrics click
        private void buttonMetrics_Click(object sender, EventArgs e)
        {
            //This will open the config form
            Metrics metrics = new Metrics();
            metrics.Show();
        }

        // Code to run on Exit click
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
