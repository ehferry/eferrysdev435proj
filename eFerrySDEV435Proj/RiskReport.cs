﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file connected to the risk report screen of the loan risk calculator app. 
 * 
 */

// Includes
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eFerrySDEV435Proj
{
    public partial class RiskReport : Form
    {
        // Form Variables
        string 
         borrowTotal,
         lendTotal,
         loanTotal,
         riskTotal,
         borrowMess,
         lendMess,
         loanMess,
         calctype;

        public RiskReport()
        {
            InitializeComponent();

            calctype = Calculation.getCalcType();

            populateReport();
            setMessages();
        }

        /*
        * Method Name: isInteger
        * Description: Returns true if the parameter is an integer
        */
        private void populateReport()
        {

            // TODO: Determine how to handle bad grab
            try
            {
                if (calctype == "full")
                {
                    textBoxCredScore.Text = Calculation.getCreditScore();
                    textBoxGrossIncome.Text = Calculation.getGrossIncome();
                    textBoxEmployStat.Text = Calculation.getEmployment();

                    textBoxDebtOnFile.Text = Calculation.getDebt();
                    textBoxDepOnFile.Text = Calculation.getDebit();

                    textBoxMinorRes.Text = Calculation.getMinor();
                    textBoxIntRes.Text = Calculation.getInterest();
                    textBoxBalRes.Text = Calculation.getBalance();
                    textBoxUnSecRes.Text = Calculation.getUnsecured();
                    textBoxCollatRes.Text = Calculation.getCollateral();
                    textBoxLienRes.Text = Calculation.getLien();
                    textBoxCoSignRes.Text = Calculation.getCosigned();

                    borrowTotal = Calculation.getBorrowResult();
                    loanTotal = Calculation.getLoanResult();
                    lendTotal = Calculation.getLendResult();

                    textBoxBorrowerResult.Text = borrowTotal;
                    textBoxLendingResult.Text = lendTotal;
                    textBoxLoanInfoResult.Text = loanTotal;

                    riskTotal = Calculation.getFullResult(borrowTotal, loanTotal, lendTotal);

                    labelResult.Text = riskTotal;
                }
                else if(calctype == "fullLIL")
                {
                    textBoxCredScore.Text = "Not calculated";
                    textBoxGrossIncome.Text = "Not calculated";
                    textBoxEmployStat.Text = "Not calculated";

                    textBoxDebtOnFile.Text = Calculation.getDebt();
                    textBoxDepOnFile.Text = Calculation.getDebit();

                    textBoxMinorRes.Text = Calculation.getMinor();
                    textBoxIntRes.Text = Calculation.getInterest();
                    textBoxBalRes.Text = Calculation.getBalance();
                    textBoxUnSecRes.Text = Calculation.getUnsecured();
                    textBoxCollatRes.Text = Calculation.getCollateral();
                    textBoxLienRes.Text = Calculation.getLien();
                    textBoxCoSignRes.Text = Calculation.getCosigned();

                    loanTotal = Calculation.getLoanResult();
                    lendTotal = Calculation.getLendResult();

                    textBoxLendingResult.Text = lendTotal;
                    textBoxLoanInfoResult.Text = loanTotal;

                    riskTotal = Calculation.getFullLILResult(loanTotal, lendTotal);

                    labelResult.Text = riskTotal;
                }
                else if(calctype == "part")
                {
                    textBoxCredScore.Text = Calculation.getCreditScore();
                    textBoxGrossIncome.Text = Calculation.getGrossIncome();
                    textBoxEmployStat.Text = Calculation.getEmployment();

                    textBoxDebtOnFile.Text = "Not calculated";
                    textBoxDepOnFile.Text = "Not calculated";

                    textBoxMinorRes.Text = Calculation.getMinor();
                    textBoxIntRes.Text = Calculation.getInterest();
                    textBoxBalRes.Text = Calculation.getBalance();
                    textBoxUnSecRes.Text = Calculation.getUnsecured();
                    textBoxCollatRes.Text = Calculation.getCollateral();
                    textBoxLienRes.Text = Calculation.getLien();
                    textBoxCoSignRes.Text = Calculation.getCosigned();

                    borrowTotal = Calculation.getBorrowResult();
                    loanTotal = Calculation.getLoanResult();

                    textBoxBorrowerResult.Text = borrowTotal;
                    textBoxLoanInfoResult.Text = loanTotal;

                    riskTotal = Calculation.getPartResult(borrowTotal, loanTotal);

                    labelResult.Text = riskTotal;
                }
                else // partLIL
                {
                    textBoxCredScore.Text = "Not calculated";
                    textBoxGrossIncome.Text = "Not calculated";
                    textBoxEmployStat.Text = "Not calculated";

                    textBoxDebtOnFile.Text = "Not calculated";
                    textBoxDepOnFile.Text = "Not calculated";

                    textBoxMinorRes.Text = Calculation.getMinor();
                    textBoxIntRes.Text = Calculation.getInterest();
                    textBoxBalRes.Text = Calculation.getBalance();
                    textBoxUnSecRes.Text = Calculation.getUnsecured();
                    textBoxCollatRes.Text = Calculation.getCollateral();
                    textBoxLienRes.Text = Calculation.getLien();
                    textBoxCoSignRes.Text = Calculation.getCosigned();

                    loanTotal = Calculation.getLoanResult();

                    textBoxLoanInfoResult.Text = loanTotal;

                    riskTotal = Calculation.getPartLILResult(loanTotal);

                    labelResult.Text = riskTotal;
                }

            }
            catch (Exception ex)
            {
                // Handle any error when getting the values
            }
        }

        /*
        * Method Name: setMessages
        * Description: Sets the message values to be displayed
        */
        private void setMessages()
        {

            if (calctype == "full")
            {
                borrowMess = Calculation.getCreditScoreMess() +
                    Calculation.getGrossIncomeMess() +
                    Calculation.getEmploymentMess();

                textBoxBorrowSuggest.Text = borrowMess;

                lendMess = Calculation.getDebtMess() + Calculation.getDebitMess();
                textBoxHistorySuggest.Text = lendMess;

                loanMess = Calculation.getMinorMess() +
                    Calculation.getInterestMess() +
                    Calculation.getBalanceMess() +
                    Calculation.getUnsecuredMess() +
                    Calculation.getCollateralMess() +
                    Calculation.getLienMess() +
                    Calculation.getCosignedMess();

                textBoxLoanSuggest.Text = loanMess;
            }
            else if(calctype == "fullLIL")
            {
                lendMess = Calculation.getDebtMess() + Calculation.getDebitMess();
                textBoxHistorySuggest.Text = lendMess;

                loanMess = Calculation.getMinorMess() +
                    Calculation.getInterestMess() +
                    Calculation.getBalanceMess() +
                    Calculation.getUnsecuredMess() +
                    Calculation.getCollateralMess() +
                    Calculation.getLienMess() +
                    Calculation.getCosignedMess();

                textBoxLoanSuggest.Text = loanMess;
            }
            else if (calctype == "part")
            {
                borrowMess = Calculation.getCreditScoreMess() +
                    Calculation.getGrossIncomeMess() +
                    Calculation.getEmploymentMess();

                textBoxBorrowSuggest.Text = borrowMess;

                loanMess = Calculation.getMinorMess() +
                    Calculation.getInterestMess() +
                    Calculation.getBalanceMess() +
                    Calculation.getUnsecuredMess() +
                    Calculation.getCollateralMess() +
                    Calculation.getLienMess() +
                    Calculation.getCosignedMess();

                textBoxLoanSuggest.Text = loanMess;
            }
            else // partLIL
            {
                loanMess = Calculation.getMinorMess() +
                    Calculation.getInterestMess() +
                    Calculation.getBalanceMess() +
                    Calculation.getUnsecuredMess() +
                    Calculation.getCollateralMess() +
                    Calculation.getLienMess() +
                    Calculation.getCosignedMess();

                textBoxLoanSuggest.Text = loanMess;
            }
        }

        /*
        * Method Name: buttonClose_Click
        * Description: What happens when the user clicks close on the form? Chance to say Yes/No?
        */
        private void buttonClose_Click(object sender, EventArgs e)
        {
            //This will open the main form and close this form
            this.Hide();
            MainForm main = new MainForm();
            main.Closed += (s, args) => this.Close();
            main.Show();
        }
    }
}
