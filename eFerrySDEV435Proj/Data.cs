﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file which contains all methods associated with getting data from the database
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace eFerrySDEV435Proj
{
    /*
    * Class Name: Data
    * Description: Contains all methods that touch the database
    */
    class Data
    {
        private string connString;
        private SqlConnection conn;
        private SqlCommand command;
        private SqlDataAdapter adapter = new SqlDataAdapter();


        /*
        * Method Name: setConnString
        * Description: Returns the connection string for the database connected to this project
        */
        public void setConnString()
        {
            // Grab the connection string from the app.config file
            connString = System.Configuration.ConfigurationManager.ConnectionStrings["SDEV435ProjConnectionString"].ToString();
        }

        /*
        * Method Name: openData
        * Description: Open the database to grab data
        */
        public bool openData()
        {
            try
            {
                // Open the connection
                conn = new SqlConnection(connString);
                conn.Open();
            }
            catch
            {
                return false;
            }

            return true;
        }

        /*
        * Method Name: closeData
        * Description: Close the database
        */
        public bool closeData()
        {
            try
            {
                // Close the connection
                conn.Close();
            }
            catch
            {
                return false;
            }

            return true;
        }

        /*
        * Method Name: getEmploymentStat
        * Description: Returns the employment status options from the database
        */
        public DataTable getEmploymentStat()
        {
            DataTable data = new DataTable();

            string sql = "SELECT employmentstatusdesc, riskrating from employmentstatus";

            command = conn.CreateCommand();

            command.CommandText = sql;

            adapter.SelectCommand = command;

            adapter.Fill(data);

            command.Dispose();

            return data;
        }

        /*
        * Method Name: getLoanMinor
        * Description: Returns the loan minor options from the database
        */
        public DataTable getLoanMinor()
        {
            DataTable data = new DataTable();

            string sql = "SELECT minordesc, riskrating from minor";

            command = conn.CreateCommand();

            command.CommandText = sql;

            adapter.SelectCommand = command;

            adapter.Fill(data);

            command.Dispose();

            return data;
        }

        /*
        * Method Name: isPerson
        * Description: Returns true if the person number exists in the system
        */
        public bool isPerson(int persnbr)
        {
            DataSet data = new DataSet();

            string sql = "SELECT * from PERS where persnbr = " + persnbr;

            command = conn.CreateCommand();

            command.CommandText = sql;

            adapter.SelectCommand = command;

            adapter.Fill(data);

            command.Dispose();

            if (data.Tables.Count > 0)
            {
                if (data.Tables[0].Rows.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
                //Something may have gone wrong if we get no tables
            }
        }

        /*
        * Method Name: getPerson
        * Description: Returns the debt on file and debit on file values for the desired person
        */
        public DataTable getPerson(int persnbr)
        {
            DataTable data = new DataTable();

            string sql = "SELECT debtonfile, debitonfile from PERS where persnbr = " + persnbr;

            command = conn.CreateCommand();

            command.CommandText = sql;

            adapter.SelectCommand = command;

            adapter.Fill(data);

            command.Dispose();

            return data;
        }
    }
}
