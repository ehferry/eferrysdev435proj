﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file containing the calculation class used in the loan risk calculator app. 
 * 
 */

// Includes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eFerrySDEV435Proj
{

    /*
    * Class Name: Calculation
    * Description: Contains all methods for calculation 
    */
    class Calculation
    {
        // Contains: Calculations; Config checks; Shared Screen value get set
        static string creditScore,
            grossIncome,
            employment,
            minor,
            interest,
            balance,
            unsecured,
            collateral,
            lien,
            cosign,
            creditScoreMess,
            grossIncomeMess,
            employmentMess,
            minorMess,
            interestMess,
            balanceMess,
            unsecuredMess,
            collateralMess,
            lienMess,
            cosignMess,
            debtMess,
            debitMess,
            debt,
            debit,
            calctyp;

        /*
        * Method Name: creditScoreRate
        * Description: Returns a rating depending on the credit score
        */
        static private string creditScoreRate(int score)
        {
            string rating;

            // Set calculation message used on risk report screen
            creditScoreMess = "The credit score " + score + " results in a risk rating of ";

            if (score >= 700)
            {
                rating = "Green";

                creditScoreMess += rating + ". Nothing can be done to improve this rating.\n";
            }
            else if (score < 700 & score > 600)
            {
                rating = "Yellow";

                creditScoreMess += rating + ". The credit score must be raised above 700 to be considered Green.\n";
            }
            else
            {
                rating = "Red";

                creditScoreMess += rating + ". The credit score must be raised above 600 to be considered Yellow.\n";
            }



            return rating;
        }

        /*
        * Method Name: grossIncomeRate
        * Description: Returns a rating depending on the gross income
        */
        static private string grossIncomeRate(int income)
        {
            string rating;

            // Set calculation message used on risk report screen
            grossIncomeMess = "The gross income of $" + income + " results in a risk rating of ";

            if (income >= 100000)
            {
                rating = "Green";

                grossIncomeMess += rating + ". Nothing can be done to improve this rating.\n";
            }
            else if (income < 100000 & income > 50000)
            {
                rating = "Yellow";

                grossIncomeMess += rating + ". The total gross income must be raised above $100,000 to be considered Green.\n";
            }
            else
            {
                rating = "Red";

                grossIncomeMess += rating + ". The total gross income must be raised above $50,000 to be considered Yellow.\n";
            }

            return rating;
        }

        /*
        * Method Name: employStatRate
        * Description: Returns a rating depending on the employment status
        */
        static private string employStatRate(string employStat)
        {
            string rating;

            if (employStat == "G")
            {
                rating = "Green";
            }
            else if (employStat == "Y")
            {
                rating = "Yellow";
            }
            else
            {
                rating = "Red";
            }

            employmentMess = "The employment status risk rating is determined by your company data. Refer to the stored database values for more information.\n";

            return rating;
        }

        /*
        * Method Name: minorRate
        * Description: Returns a rating depending on the minor
        */
        static private string minorRate(string minor)
        {
            string rating;

            if(minor == "G")
            {
                rating = "Green";
            }
            else if(minor == "Y")
            {
                rating = "Yellow";
            }
            else
            {
                rating = "Red";
            }

            minorMess = "The minor code risk rating is determined by your company data. Refer to the stored database values for more information.\n";

            return rating;
        }

        /*
        * Method Name: interestRate
        * Description: Returns a rating depending on the interest rate
        */
        static private string interestRate(double intRate)
        {
            string rating = String.Empty;

            interestMess = "The interest rate " + intRate + " results in a rating of ";

            if (intRate >= 30)
            {
                rating = "Red";

                interestMess += rating + ". The rate must be less than 30.00% to be considered Yellow.\n";
            }
            else if (intRate < 30 & intRate > 18)
            {
                rating = "Yellow";

                interestMess += rating + ". The rate must be less than 18.00% to be considered Green.\n";
            }
            else
            {
                rating = "Green";

                interestMess += rating + ". This rating cannot be improved.\n";
            }

            return rating;
        }

        /*
        * Method Name: balRate
        * Description: Returns a rating depending on the loan balance
        */
        static private string balRate(int bal)
        {
            string rating = String.Empty;

            balanceMess = "A loan balance of $" + bal + " results in a rating of ";

            if (bal >= 100000)
            {
                rating = "Red";

                balanceMess += rating + ". The loan balance must be less than $100,000 to be considered Yellow.\n";
            }
            else if (bal < 100000 & bal > 65000)
            {
                rating = "Yellow";

                balanceMess += rating + ". The loan balnce must be less than $65,000 to be considered Green.\n";
            }
            else
            {
                rating = "Green";

                balanceMess += rating + ". Nothing can be done to improve this rating.\n";
            }

            return rating;
        }

        /*
        * Method Name: unsecuredRate
        * Description: Returns a rating depending on if the loan is unsecured
        */
        static private string unsecuredRate(bool unSecuredYN)
        {
            string rating = String.Empty;

            if (!unSecuredYN)
            {
                rating = "Green";

                unsecuredMess = "A secured loan results in a Green rating. This rating cannot be improved.\n";
            }
            else
            {
                rating = "Red";

                unsecuredMess = "An unsecured loan results in a Red rating. A loan must be secured for this to be considered Green.\n";
            }

            return rating;
        }

        /*
        * Method Name: collateralRate
        * Description: Returns a rating depending on the collateral amount in relation to the loan balance
        */
        static private string collateralRate(double collat, double loanBal)
        {
            string rating = String.Empty;

            double percentage = (collat / loanBal * 100);

            collateralMess = "A collateral balance of $" + collat + " results in a rating of ";

            if (percentage >= 75)
            {
                rating = "Green";

                collateralMess += rating + ". This rating cannot be improved.\n";
            }
            else if (percentage < 75 & percentage >= 45)
            {
                rating = "Yellow";

                collateralMess += rating + ". The percentage of the loan covered by collateral must be at least 75% to be considered Green.\n";
            }
            else
            {
                rating = "Red";

                collateralMess += rating + ". The percentage of the loan covered by collateral must be at least 45% to be considered Yellow.\n";
            }

            return rating;
        }

        /*
        * Method Name: lienRate
        * Description: Returns a rating depending on the lien amount in relation to the loan balance
        */
        static private string lienRate(int lien, int loanBal)
        {
            string rating = String.Empty;

            double percentage = lien / loanBal * 100;

            lienMess = "A lien rate of " + lien + " results in a rating of ";

            if (percentage >= 50)
            {
                rating = "Red";

                lienMess += rating + ". The percentage of the loan promised in lien must be less than 50% to be considered Yellow.\n";
            }
            else if (percentage < 50 & percentage >= 25)
            {
                rating = "Yellow";

                lienMess += rating + ". The percentage of the loan promised in lien must be less than 25% to be considered Green.\n";
            }
            else
            {
                rating = "Green";

                lienMess += rating + ". This rating cannot be improved.\n";
            }

            return rating;
        }

        /*
        * Method Name: cosignRate
        * Description: Returns a rating depending on the presence of a cosigner
        */
        static private string cosignRate(bool cosignerYN)
        {
            string rating = String.Empty;        

            if (cosignerYN)
            {
                rating = "Green";

                cosignMess = "A cosigned loan results in a Green rating. This rating cannot be improved.\n";
            }
            else
            {
                rating = "Red";

                cosignMess = "A loan without a cosigner results in a Red rating. A loan must have a cosigner for this to be considered Green.\n";
            }

            return rating;
        }

        /*
        * Method Name: debtRate
        * Description: Returns a rating depending on the presence of a cosigner
        */
        static private string debtRate(int debtonfile, int debitonfile)
        {
            string rating = String.Empty;

            if (debitonfile > debtonfile)
            {
                rating = "Green";

                debtMess = "Deposit on file amount exceeds the debt on file and results in a Green rating. This rating cannot be improved.\n";
            }
            else
            {
                rating = "Red";

                debtMess = "Debt on file exceeds the deposit on file amount and results in a Red rating. A person must have a debit balance exceeding their debt balance to be considered Green.\n";
            }

            return rating;
        }

        /*
        * Method Name: debitRate
        * Description: Returns a rating depending on the presence of a cosigner
        */
        static private string debitRate(int debitonfile, int loanbal)
        {
            string rating = String.Empty;

            if (debitonfile > loanbal)
            {
                rating = "Green";

                debitMess = "Deposit on file amount exceeds the loan balance and results in a Green rating. This rating cannot be improved.\n";
            }
            else
            {
                rating = "Red";

                debitMess = "Loan balance exceeds the deposit on file amount and results in a Red rating. A person must have a debit balance exceeding the loan balance to be considered Green.\n";
            }

            return rating;
        }


        //--------------------------------------------------------------------------
        //----- Set Risk Report Values
        //--------------------------------------------------------------------------
        /*
        * Method Name: setCalcType
        * Description: Sets the calctyp to be used for this report
        */
        static public void setCalcType(string _calctyp)
        {
            calctyp = _calctyp;
        }

        /*
        * Method Name: setDebt
        * Description: Sets the debt rating value by passing the debtonfile amount to the private calculation method
        */
        static public void setDebt(int _debt, int _debit)
        {
            debt = debtRate(_debt, _debit);
        }

        /*
        * Method Name: setDeposit
        * Description: Sets the deposit rating value by passing the debit on file amount and loan balance to the private calculation method
        */
        static public void setDeposit(int _debit, int loanbal)
        {
            debit = debitRate(_debit, loanbal);
        }


        /*
        * Method Name: setCreditScore
        * Description: Sets the credit score rating value by passing the credit score to the private calculation method
        */
        static public void setCreditScore(int score)
        {
            creditScore = creditScoreRate(score);
        }

        /*
        * Method Name: setGrossIncome
        * Description: Sets the gross income rating value by passing the gross income to the private calculation method
        */
        static public void setGrossIncome(int income)
        {
            grossIncome = grossIncomeRate(income);
        }

        /*
        * Method Name: setEmployment
        * Description: Sets the employment status rating value by passing the employment status to the private calculation method
        */
        static public void setEmployment(string employStat)
        {
            employment = employStatRate(employStat);
        }

        /*
        * Method Name: setMinor
        * Description: Sets the minor rating value by passing the minor to the private calculation method
        */
        static public void setMinor(string minorCd)
        {
            minor = minorRate(minorCd);
        }

        /*
        * Method Name: setInterest
        * Description: Sets the interest rating value by passing the interest to the private calculation method
        */
        static public void setInterest(double intRate)
        {
            interest = interestRate(intRate);
        }

        /*
        * Method Name: setBalance
        * Description: Sets the balance rating value by passing the balance to the private calculation method
        */
        static public void setBalance(int bal)
        {
            balance = balRate(bal);
        }

        /*
        * Method Name: setUnsecured
        * Description: Sets the unsecured rating value by passing the unsecured bool to the private calculation method
        */
        static public void setUnsecured(bool unSecuredYN)
        {
            unsecured = unsecuredRate(unSecuredYN);
        }

        /*
        * Method Name: setCollateral
        * Description: Sets the collateral rating value by passing the collateral and loan balance to the private calculation method
        */
        static public void setCollateral(int collat, int loanBal)
        {
            collateral = collateralRate(collat, loanBal);
        }

        /*
        * Method Name: setLien
        * Description: Sets the lien rating value by passing the lien and loan balance to the private calculation method
        */
        static public void setLien(int lienAmt, int loanBal)
        {
            lien = lienRate(lienAmt, loanBal);
        }

        /*
        * Method Name: setCosigned
        * Description: Sets the cosigned rating value by passing the cosigner bool to the private calculation method
        */
        static public void setCosigned(bool cosignerYN)
        {
            cosign = cosignRate(cosignerYN);
        }

        //------------------------------------------------------
        //----------- Get Risk Report Values
        //------------------------------------------------------
        /*
        * Method Name: getCalcType
        * Description: Returns the calctyp to be used for this report
        */
        static public string getCalcType()
        {
            return calctyp;
        }

        /*
        * Method Name: getDebt
        * Description: Returns debt rating value use in the Risk Report screen
        */
        static public string getDebt()
        {
            return debt;
        }

        /*
        * Method Name: getDebit
        * Description: Returns debt rating value use in the Risk Report screen
        */
        static public string getDebit()
        {
            return debit;
        }

        /*
        * Method Name: getCreditScore
        * Description: Gets the credit score rating value for use in the Risk Report screen
        */
        static public string getCreditScore()
        {
            return creditScore;
        }

        /*
        * Method Name: getGrossIncome
        * Description: Gets the gross income rating value for use in the Risk Report screen
        */
        static public string getGrossIncome()
        {
            return grossIncome;
        }

        /*
        * Method Name: getEmployment
        * Description: Gets the employment status rating value for use in the Risk Report screen
        */
        static public string getEmployment()
        {
            return employment;
        }

        /*
        * Method Name: getMinor
        * Description: Gets the minor rating value for use in the Risk Report screen
        */
        static public string getMinor()
        {
            return minor;
        }

        /*
        * Method Name: getInterest
        * Description: Gets the interest rating value for use in the Risk Report screen
        */
        static public string getInterest()
        {
            return interest;
        }

        /*
        * Method Name: getBalance
        * Description: Gets the balance rating value for use in the Risk Report screen
        */
        static public string getBalance()
        {
            return balance;
        }

        /*
        * Method Name: getUnsecured
        * Description: Gets the unsecured rating value for use in the Risk Report screen
        */
        static public string getUnsecured()
        {
            return unsecured;
        }

        /*
        * Method Name: getCollateral
        * Description: Gets the collateral rating value for use in the Risk Report screen
        */
        static public string getCollateral()
        {
            return collateral;
        }

        /*
        * Method Name: getLien
        * Description: Gets the lien rating value for use in the Risk Report screen
        */
        static public string getLien()
        {
            return lien;
        }

        /*
        * Method Name: getCosigned
        * Description: Gets the cosigned rating value for use in the Risk Report screen
        */
        static public string getCosigned()
        {
            return cosign;
        }

        /*
        * Method Name: getBorrowResult
        * Description: Gets the Borrower Section rating value for use in the Risk Report screen from the Credit Score, Gross Income, annd Employment Status
        */
        static public string getBorrowResult()
        {

            int numVal = (Calculation.checkVal(creditScore) + Calculation.checkVal(grossIncome) + Calculation.checkVal(employment));

            if (numVal >= 6)
            {
                return "Green";
            }
            else if (numVal < 6 || numVal >= 3)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
        * Method Name: getLoanResult
        * Description: Gets the Loan Section rating value for use in the Risk Report screen from the minor, interest rate, balance, unsecured, collateral, lien, and cosign
        */
        static public string getLoanResult()
        {

            int numVal = (Calculation.checkVal(minor) + Calculation.checkVal(interest) + Calculation.checkVal(balance) + Calculation.checkVal(unsecured) + Calculation.checkVal(collateral) + Calculation.checkVal(lien) + Calculation.checkVal(cosign));

            if (numVal == 14 || numVal >= 12)
            {
                return "Green";
            }
            else if (numVal < 12 || numVal >= 5)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
        * Method Name: getLendResult
        * Description: Gets the Lending History Section rating value for use in the Risk Report screen from the Deposit On File and Debt on File
        */
        static public string getLendResult()
        {

            int numVal = (Calculation.checkVal(debt) + Calculation.checkVal(debit));

            if (numVal == 4)
            {
                return "Green";
            }
            else if (numVal < 4 || numVal >= 2)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
        * Method Name: getFullResult
        * Description: Gets the full report rating value with person values for use in the Risk Report screen from the three main sections
        */
        static public string getFullResult(string borrow, string loan, string lend)
        {
            // Redo

            int numVal = (Calculation.checkVal(borrow) + Calculation.checkVal(loan) + Calculation.checkVal(lend));

            if (numVal >= 6)
            {
                return "Green";
            }
            else if (numVal < 6 || numVal >= 4)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
        * Method Name: getPartResult
        * Description: Gets the partial report rating value for use in the Risk Report screen from the three main sections
        */
        static public string getPartResult(string borrow, string loan)
        {
            //Redo

            int numVal = (Calculation.checkVal(borrow) + Calculation.checkVal(loan));

            if (numVal >= 4)
            {
                return "Green";
            }
            else if (numVal < 4 || numVal >= 3)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
        * Method Name: getFullLILResult
        * Description: Gets the full LIL report rating value with person values for use in the Risk Report screen from the two sections
        */
        static public string getFullLILResult(string loan, string lend)
        {
            // Redo

            int numVal = (Calculation.checkVal(loan) + Calculation.checkVal(lend));

            if (numVal >= 4)
            {
                return "Green";
            }
            else if (numVal < 4 || numVal >= 3)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
       * Method Name: getPartLILResult
       * Description: Gets the partial LIL report rating value for use in the Risk Report screen from the two sections
       */
        static public string getPartLILResult(string loan)
        {
            // Redo

            int numVal = (Calculation.checkVal(loan));

            if (numVal >= 2)
            {
                return "Green";
            }
            else if (numVal < 2 || numVal >= 1)
            {
                return "Yellow";
            }
            else
            {
                return "Red";
            }
        }

        /*
        * Method Name: checkval
        * Description: Returns an int value based on the rating given for the category
        */
        static private int checkVal(string rate)
        {
            if (rate == "Green")
            {
                return 2;
            }
            else if (rate == "Yellow")
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        
        //--------------------------------------------------------------------------
        //----- Get Risk Report Calculation Messages
        //--------------------------------------------------------------------------
        static public string getCreditScoreMess()
        {
            return creditScoreMess;
        }

        /*
        * Method Name: getGrossIncome
        * Description: Gets the gross income rating value for use in the Risk Report screen
        */
        static public string getGrossIncomeMess()
        {
            return grossIncomeMess;
        }

        /*
        * Method Name: getEmployment
        * Description: Gets the employment status rating value for use in the Risk Report screen
        */
        static public string getEmploymentMess()
        {
            return employmentMess;
        }

        /*
        * Method Name: getMinor
        * Description: Gets the minor rating value for use in the Risk Report screen
        */
        static public string getMinorMess()
        {
            return minorMess;
        }

        /*
        * Method Name: getInterest
        * Description: Gets the interest rating value for use in the Risk Report screen
        */
        static public string getInterestMess()
        {
            return interestMess;
        }

        /*
        * Method Name: getBalance
        * Description: Gets the balance rating value for use in the Risk Report screen
        */
        static public string getBalanceMess()
        {
            return balanceMess;
        }

        /*
        * Method Name: getUnsecured
        * Description: Gets the unsecured rating value for use in the Risk Report screen
        */
        static public string getUnsecuredMess()
        {
            return unsecuredMess;
        }

        /*
        * Method Name: getCollateral
        * Description: Gets the collateral rating value for use in the Risk Report screen
        */
        static public string getCollateralMess()
        {
            return collateralMess;
        }

        /*
        * Method Name: getLien
        * Description: Gets the lien rating value for use in the Risk Report screen
        */
        static public string getLienMess()
        {
            return lienMess;
        }

        /*
        * Method Name: getCosigned
        * Description: Gets the cosigned rating value for use in the Risk Report screen
        */
        static public string getCosignedMess()
        {
            return cosignMess;
        }

        /*
        * Method Name: getDebtMess
        * Description: Gets the debtonfile rating value for use in the Risk Report screen
        */
        static public string getDebtMess()
        {
            return debtMess;
        }

        /*
        * Method Name: getDebitMess
        * Description: Gets the debitonfile rating value for use in the Risk Report screen
        */
        static public string getDebitMess()
        {
            return debitMess;
        }
    }
}
