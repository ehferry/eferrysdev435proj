﻿namespace eFerrySDEV435Proj
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculator));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCreditScore = new System.Windows.Forms.TextBox();
            this.textBoxIncome = new System.Windows.Forms.TextBox();
            this.comboBoxEmployStat = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panelBorrower = new System.Windows.Forms.Panel();
            this.panelLending = new System.Windows.Forms.Panel();
            this.buttonRemovePers = new System.Windows.Forms.Button();
            this.textBoxPersonStat = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPersonNbr = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonConfirmPers = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxCoSign = new System.Windows.Forms.CheckBox();
            this.checkBoxUnsecure = new System.Windows.Forms.CheckBox();
            this.textBoxCollateral = new System.Windows.Forms.TextBox();
            this.textBoxLien = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxMinor = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxInt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxBal = new System.Windows.Forms.TextBox();
            this.buttonCalc = new System.Windows.Forms.Button();
            this.buttonCancelClose = new System.Windows.Forms.Button();
            this.panelBorrower.SuspendLayout();
            this.panelLending.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Borrower Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Credit Score";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Gross Income";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Employment Status";
            // 
            // textBoxCreditScore
            // 
            this.textBoxCreditScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCreditScore.Location = new System.Drawing.Point(142, 66);
            this.textBoxCreditScore.MaxLength = 3;
            this.textBoxCreditScore.Name = "textBoxCreditScore";
            this.textBoxCreditScore.Size = new System.Drawing.Size(121, 20);
            this.textBoxCreditScore.TabIndex = 5;
            // 
            // textBoxIncome
            // 
            this.textBoxIncome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIncome.Location = new System.Drawing.Point(142, 108);
            this.textBoxIncome.MaxLength = 9;
            this.textBoxIncome.Name = "textBoxIncome";
            this.textBoxIncome.Size = new System.Drawing.Size(121, 20);
            this.textBoxIncome.TabIndex = 6;
            // 
            // comboBoxEmployStat
            // 
            this.comboBoxEmployStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEmployStat.FormattingEnabled = true;
            this.comboBoxEmployStat.Location = new System.Drawing.Point(142, 150);
            this.comboBoxEmployStat.Name = "comboBoxEmployStat";
            this.comboBoxEmployStat.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEmployStat.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(190, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Lending History";
            // 
            // panelBorrower
            // 
            this.panelBorrower.Controls.Add(this.label1);
            this.panelBorrower.Controls.Add(this.label3);
            this.panelBorrower.Controls.Add(this.comboBoxEmployStat);
            this.panelBorrower.Controls.Add(this.label4);
            this.panelBorrower.Controls.Add(this.textBoxIncome);
            this.panelBorrower.Controls.Add(this.label5);
            this.panelBorrower.Controls.Add(this.textBoxCreditScore);
            this.panelBorrower.Location = new System.Drawing.Point(12, 18);
            this.panelBorrower.Name = "panelBorrower";
            this.panelBorrower.Size = new System.Drawing.Size(520, 204);
            this.panelBorrower.TabIndex = 9;
            // 
            // panelLending
            // 
            this.panelLending.Controls.Add(this.buttonRemovePers);
            this.panelLending.Controls.Add(this.textBoxPersonStat);
            this.panelLending.Controls.Add(this.label8);
            this.panelLending.Controls.Add(this.textBoxPersonNbr);
            this.panelLending.Controls.Add(this.label7);
            this.panelLending.Controls.Add(this.buttonConfirmPers);
            this.panelLending.Controls.Add(this.label6);
            this.panelLending.Location = new System.Drawing.Point(12, 238);
            this.panelLending.Name = "panelLending";
            this.panelLending.Size = new System.Drawing.Size(520, 191);
            this.panelLending.TabIndex = 10;
            // 
            // buttonRemovePers
            // 
            this.buttonRemovePers.Location = new System.Drawing.Point(357, 143);
            this.buttonRemovePers.Name = "buttonRemovePers";
            this.buttonRemovePers.Size = new System.Drawing.Size(75, 23);
            this.buttonRemovePers.TabIndex = 14;
            this.buttonRemovePers.Text = "Remove";
            this.buttonRemovePers.UseVisualStyleBackColor = true;
            this.buttonRemovePers.Click += new System.EventHandler(this.buttonRemovePers_Click);
            // 
            // textBoxPersonStat
            // 
            this.textBoxPersonStat.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.textBoxPersonStat.Location = new System.Drawing.Point(11, 145);
            this.textBoxPersonStat.Name = "textBoxPersonStat";
            this.textBoxPersonStat.ReadOnly = true;
            this.textBoxPersonStat.Size = new System.Drawing.Size(324, 20);
            this.textBoxPersonStat.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 15);
            this.label8.TabIndex = 11;
            this.label8.Text = "Person Number";
            // 
            // textBoxPersonNbr
            // 
            this.textBoxPersonNbr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPersonNbr.Location = new System.Drawing.Point(116, 101);
            this.textBoxPersonNbr.MaxLength = 9;
            this.textBoxPersonNbr.Name = "textBoxPersonNbr";
            this.textBoxPersonNbr.Size = new System.Drawing.Size(121, 20);
            this.textBoxPersonNbr.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Modern No. 20", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(257, 14);
            this.label7.TabIndex = 10;
            this.label7.Text = "Enter Person Number to Look Up Values for Calculation";
            // 
            // buttonConfirmPers
            // 
            this.buttonConfirmPers.Location = new System.Drawing.Point(260, 99);
            this.buttonConfirmPers.Name = "buttonConfirmPers";
            this.buttonConfirmPers.Size = new System.Drawing.Size(75, 23);
            this.buttonConfirmPers.TabIndex = 9;
            this.buttonConfirmPers.Text = "Confirm";
            this.buttonConfirmPers.UseVisualStyleBackColor = true;
            this.buttonConfirmPers.Click += new System.EventHandler(this.buttonConfirmPers_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxCoSign);
            this.panel1.Controls.Add(this.checkBoxUnsecure);
            this.panel1.Controls.Add(this.textBoxCollateral);
            this.panel1.Controls.Add(this.textBoxLien);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.comboBoxMinor);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBoxInt);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.textBoxBal);
            this.panel1.Location = new System.Drawing.Point(12, 445);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(520, 377);
            this.panel1.TabIndex = 11;
            // 
            // checkBoxCoSign
            // 
            this.checkBoxCoSign.AutoSize = true;
            this.checkBoxCoSign.Location = new System.Drawing.Point(86, 337);
            this.checkBoxCoSign.Name = "checkBoxCoSign";
            this.checkBoxCoSign.Size = new System.Drawing.Size(15, 14);
            this.checkBoxCoSign.TabIndex = 22;
            this.checkBoxCoSign.UseVisualStyleBackColor = true;
            // 
            // checkBoxUnsecure
            // 
            this.checkBoxUnsecure.AutoSize = true;
            this.checkBoxUnsecure.Location = new System.Drawing.Point(86, 218);
            this.checkBoxUnsecure.Name = "checkBoxUnsecure";
            this.checkBoxUnsecure.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUnsecure.TabIndex = 21;
            this.checkBoxUnsecure.UseVisualStyleBackColor = true;
            this.checkBoxUnsecure.CheckedChanged += new System.EventHandler(this.checkBoxUnsecure_CheckedChanged);
            // 
            // textBoxCollateral
            // 
            this.textBoxCollateral.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCollateral.Location = new System.Drawing.Point(142, 254);
            this.textBoxCollateral.MaxLength = 9;
            this.textBoxCollateral.Name = "textBoxCollateral";
            this.textBoxCollateral.Size = new System.Drawing.Size(121, 20);
            this.textBoxCollateral.TabIndex = 20;
            // 
            // textBoxLien
            // 
            this.textBoxLien.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLien.Location = new System.Drawing.Point(142, 298);
            this.textBoxLien.MaxLength = 9;
            this.textBoxLien.Name = "textBoxLien";
            this.textBoxLien.Size = new System.Drawing.Size(121, 20);
            this.textBoxLien.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 336);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 15);
            this.label16.TabIndex = 18;
            this.label16.Text = "Cosigned";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 15);
            this.label13.TabIndex = 15;
            this.label13.Text = "Unsecured";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 259);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(122, 15);
            this.label14.TabIndex = 16;
            this.label14.Text = "Collateral Amount";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 298);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 15);
            this.label15.TabIndex = 17;
            this.label15.Text = "Lien Amount";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(302, 25);
            this.label9.TabIndex = 8;
            this.label9.Text = "Pending Loan Information";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Minor";
            // 
            // comboBoxMinor
            // 
            this.comboBoxMinor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxMinor.FormattingEnabled = true;
            this.comboBoxMinor.Location = new System.Drawing.Point(141, 69);
            this.comboBoxMinor.Name = "comboBoxMinor";
            this.comboBoxMinor.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMinor.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Int Rate";
            // 
            // textBoxInt
            // 
            this.textBoxInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInt.Location = new System.Drawing.Point(142, 111);
            this.textBoxInt.MaxLength = 5;
            this.textBoxInt.Name = "textBoxInt";
            this.textBoxInt.Size = new System.Drawing.Size(121, 20);
            this.textBoxInt.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 155);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "Loan Balance";
            // 
            // textBoxBal
            // 
            this.textBoxBal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBal.Location = new System.Drawing.Point(142, 155);
            this.textBoxBal.MaxLength = 9;
            this.textBoxBal.Name = "textBoxBal";
            this.textBoxBal.Size = new System.Drawing.Size(121, 20);
            this.textBoxBal.TabIndex = 12;
            // 
            // buttonCalc
            // 
            this.buttonCalc.Location = new System.Drawing.Point(457, 838);
            this.buttonCalc.Name = "buttonCalc";
            this.buttonCalc.Size = new System.Drawing.Size(75, 23);
            this.buttonCalc.TabIndex = 12;
            this.buttonCalc.Text = "Calculate";
            this.buttonCalc.UseVisualStyleBackColor = true;
            this.buttonCalc.Click += new System.EventHandler(this.buttonCalc_Click);
            // 
            // buttonCancelClose
            // 
            this.buttonCancelClose.Location = new System.Drawing.Point(12, 838);
            this.buttonCancelClose.Name = "buttonCancelClose";
            this.buttonCancelClose.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelClose.TabIndex = 13;
            this.buttonCancelClose.Text = "Close";
            this.buttonCancelClose.UseVisualStyleBackColor = true;
            this.buttonCancelClose.Click += new System.EventHandler(this.buttonCancelClose_Click);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 873);
            this.Controls.Add(this.buttonCancelClose);
            this.Controls.Add(this.buttonCalc);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelLending);
            this.Controls.Add(this.panelBorrower);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Calculator";
            this.Text = "Calculator";
            this.panelBorrower.ResumeLayout(false);
            this.panelBorrower.PerformLayout();
            this.panelLending.ResumeLayout(false);
            this.panelLending.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCreditScore;
        private System.Windows.Forms.TextBox textBoxIncome;
        private System.Windows.Forms.ComboBox comboBoxEmployStat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelBorrower;
        private System.Windows.Forms.Panel panelLending;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPersonNbr;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonConfirmPers;
        private System.Windows.Forms.TextBox textBoxPersonStat;
        private System.Windows.Forms.Button buttonRemovePers;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxMinor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxInt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxBal;
        private System.Windows.Forms.TextBox textBoxCollateral;
        private System.Windows.Forms.TextBox textBoxLien;
        private System.Windows.Forms.CheckBox checkBoxCoSign;
        private System.Windows.Forms.CheckBox checkBoxUnsecure;
        private System.Windows.Forms.Button buttonCalc;
        private System.Windows.Forms.Button buttonCancelClose;
    }
}