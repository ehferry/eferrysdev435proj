﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file connected to the calculator screen of the loan risk calculator app. 
 * 
 */

// Includes
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eFerrySDEV435Proj
{
    public partial class Calculator : Form
    {
        // Form Variables
        int creditScore;
        int grossIncome;
        string employStat;
        int persNumber;
        string minor;
        double intRate;
        int loanBalance;
        bool unsecured;
        int collateralAmt;
        int lienAmt;
        bool cosigned;
        int debtonfile;
        int debitonfile;
        DataTable personValues;
        bool validPers;
        DataTable minorDisplay;
        DataTable employDisplay;

        // These values will come from the config file
        int credMin;
        int balMin;
        int balMax;
        string allowUnSec;
        string lowIncomeLend;

        // Form referenced classes
        Validate valid = new Validate();
        Configuration configure = new Configuration();
        Data data = new Data();


        public Calculator()
        {
            InitializeComponent();

            setMetricLimits();

            data.setConnString();
            data.openData();

            if (allowUnSec == "N")
            {
                checkBoxUnsecure.Enabled = false;
            }
            if (lowIncomeLend == "Y")
            {
                textBoxCreditScore.Enabled = false;
                textBoxIncome.Enabled = false;
                comboBoxEmployStat.Enabled = false;
            }

            validPers = false;

            setDropDowns();
        }

        /*
        * Method Name: setDropDowns
        * Description: Sets the dropdown values with the minor and employment status from the database
        */
        private void setDropDowns()
        {
            employDisplay = data.getEmploymentStat();
            minorDisplay = data.getLoanMinor();

            comboBoxMinor.DataSource = minorDisplay;
            comboBoxMinor.DisplayMember = "minordesc";

            if (lowIncomeLend == "Y")
            {

            }
            else
            {
                comboBoxEmployStat.DataSource = employDisplay;
                comboBoxEmployStat.DisplayMember = "employmentstatusdesc";
            }

        }

        /*
        * Method Name: setDropDownValues
        * Description: Sets the calculation risk values with the corresponding drop down values
        */
        private void setDropDownValues()
        {
            string test;
            // Skip employment status if low income is selected; always do minor
            foreach (DataRow row in minorDisplay.Rows)
            {

                if (comboBoxMinor.Text.ToString() == row.ItemArray[0].ToString())
                    minor = row.ItemArray[1].ToString();
            }

            if (lowIncomeLend == "N")
            {
                foreach (DataRow row in employDisplay.Rows)
                {
                    if (comboBoxEmployStat.Text.ToString() == row.ItemArray[0].ToString())
                        employStat = row.ItemArray[1].ToString();
                }
            }
            else
            {

            }
        }


        /*
        * Method Name: setMetricLimits
        * Description: Sets the values used in risk calculations that are set by the user in the metrics screen
        */
        private void setMetricLimits()
        {
            credMin = Convert.ToInt32(configure.getConfig("CRED"));
            balMin = Convert.ToInt32(configure.getConfig("LBAL"));
            balMax = Convert.ToInt32(configure.getConfig("HBAL"));

            //Check the config file to set the Allow Unsecured value
            allowUnSec = configure.getConfig("UNS");

            //Check the config file to set the Low Income Lending value
            lowIncomeLend = configure.getConfig("LIL");
        }

        /*
        * Method Name: buttonCancelClose_Click
        * Description: Closes the form is the user confirms
        */
        private void buttonCancelClose_Click(object sender, EventArgs e)
        {
            // Check if user truly wishes to close the form and end the calculation
            DialogResult closeCheck = MessageBox.Show("Are you sure you wish to end the calculation?", "End Calculation", MessageBoxButtons.OKCancel);

            if (closeCheck == DialogResult.OK)
            {
                //This will open the main form and close the calculator form
                this.Hide();
                MainForm main = new MainForm();
                main.Closed += (s, args) => this.Close();
                main.Show();
            }
        }

        /*
        * Method Name: buttonCalc_Click
        * Description: Checks the values input by the user and runs the calculations for the Risk Report screen if they are valid
        */
        private void buttonCalc_Click(object sender, EventArgs e)
        {
            if (confirmCalcValues(sender, e))
            {
                // Set report values after confirmation
                setRiskReportValues();
                //Open risk report with values
                this.Hide();
                RiskReport report = new RiskReport();
                report.Closed += (s, args) => this.Close();
                report.Show();
            }
            else
            {
                // Remain on calculator screen
            }
        }

        /*
        * Method Name: buttonConfirmPers_Click
        * Description: Returns true if the parameter is an integer
        */
        private void buttonConfirmPers_Click(object sender, EventArgs e)
        {
            // Check if person number is a valid integer
            if (valid.isInteger(textBoxPersonNbr.Text.ToString()))
            {
                persNumber = Convert.ToInt32(textBoxPersonNbr.Text.ToString());

                if (data.isPerson(persNumber))
                {
                    // Add person details
                    personValues = data.getPerson(persNumber);
                    assignPersValues();
                }
                else
                {
                    // The person number was not found in the system
                    string message = "A person record could not be found using that person number.";
                    MessageBox.Show(message);
                    return;
                }
            }
            else
            {
                //The person number is not valid
                string message = "The person number must be a valid integer.";
                MessageBox.Show(message);
                return;
            }

        }

        /*
        * Method Name: assignPersValues
        * Description: Returns true if the parameter is an integer
        */
        private void assignPersValues()
        {
            DataRow row = personValues.Rows[0];
            debtonfile = Convert.ToInt32(Convert.ToDouble(row.ItemArray.GetValue(0).ToString()));
            debitonfile = Convert.ToInt32(Convert.ToDouble(row.ItemArray.GetValue(1).ToString()));

            textBoxPersonStat.Enabled = true;
            textBoxPersonStat.Text = "The debit on file is " + debitonfile + " and the debt on file is " + debtonfile + ".";


            validPers = true;
        }


        /*
        * Method Name: buttonRemovePers_Click
        * Description: Returns true if the parameter is an integer
        */
        private void buttonRemovePers_Click(object sender, EventArgs e)
        {
            // Remove pers values and set calculations to not consider debtonfile and debitonfile
            debtonfile = 0;
            debitonfile = 0;
            textBoxPersonStat.Text = "";
            textBoxPersonNbr.Text = "";
            validPers = false;
        }

        /*
        * Method Name: confirmCalcValues
        * Description: Returns true if the user inputs for all entered values are found to be valid
        */
        private bool confirmCalcValues(object sender, EventArgs e)
        {
            setDropDownValues();

            if (lowIncomeLend == "N")
            {



                // Run value checks
                if (string.IsNullOrEmpty(textBoxCreditScore.Text.ToString()))
                {
                    // Check if user has input an interest rate
                    MessageBox.Show("The credit score must have a value to continue.");
                    return false;
                }
                else
                {
                    if (valid.isInteger(textBoxCreditScore.Text.ToString()))
                    {
                        creditScore = Int32.Parse(textBoxCreditScore.Text.ToString());

                        if (!valid.isCreditScore(creditScore, credMin))
                        {
                            //The credit score is an integer but is not in range
                            string message = "The credit score must be between " + credMin + " and 800.";
                            MessageBox.Show(message);
                            return false;

                        }
                    }
                    else
                    {
                        //The credit score is not an integer
                        string message = "The credit score must be an integer";
                        MessageBox.Show(message);
                        return false;

                    }
                }

                if (string.IsNullOrEmpty(textBoxIncome.Text.ToString()) && lowIncomeLend == "N")
                {
                    // Check if user has input an income
                    MessageBox.Show("The gross income must have a value to continue.");
                    return false;
                }
                else
                {
                    if (valid.isInteger(textBoxIncome.Text.ToString()))
                    {
                        grossIncome = Int32.Parse(textBoxIncome.Text.ToString());
                    }
                    else
                    {
                        //The gross income is not an integer
                        string message = "The income value must be an integer";
                        MessageBox.Show(message);
                        return false;
                    }
                }

                string employStat;
                if (lowIncomeLend == "N")
                {
                    // Set rating based on database value 
                    employStat = comboBoxEmployStat.SelectedItem.ToString();
                }

            }

            if (string.IsNullOrEmpty(textBoxInt.Text.ToString()))
            {
                // Check if user has input an interest rate
                MessageBox.Show("The interest rate must have a value to continue.");
                return false;
            }
            else if (valid.isIntRate(textBoxInt.Text.ToString()))
            {
                intRate = Double.Parse(textBoxInt.Text.ToString());
            }
            else
            {
                // Check if user has input a double
                MessageBox.Show("The interest rate must have a numerical value to continue. Example XX.XX. The value must be between 08.00 and 35.00");
                return false;
            }

            if (string.IsNullOrEmpty(textBoxBal.Text.ToString()))
            {
                // Check if user has input a loan balance
                MessageBox.Show("The loan balance must have a value to continue.");
                return false;
            }
            else if (!valid.isInteger(textBoxBal.Text.ToString()))
            {
                // Check if user has input an integer
                MessageBox.Show("The loan balance must have a numerical value to continue.");
                return false;
            }
            else
            {
                loanBalance = int.Parse(textBoxBal.Text.ToString());

                if (loanBalance > balMax || loanBalance < balMin)
                {
                    // Check if user has input a loan balance within the allowed range
                    MessageBox.Show("The loan balance must have a value between " + balMin + " and " + balMax);
                    return false;
                }
            }

            if (checkBoxUnsecure.Checked)
            {
                unsecured = true;
            }
            else
            {
                unsecured = false;
            }


            if (unsecured)
            {
                collateralAmt = 0;
            }
            else
            {
                if (string.IsNullOrEmpty(textBoxCollateral.Text.ToString()))
                {
                    // Check if user has input a collateral amount
                    MessageBox.Show("If the loan is secured there must be a collateral amount.");
                    return false;
                }
                else
                {
                    collateralAmt = int.Parse(textBoxCollateral.Text.ToString());

                    // Check if the collateral amount is greater than 5% of the loan balance
                    if (collateralAmt < loanBalance * 0.05)
                    {
                        // Check if the collateral amount is less than 5% of the loan balance
                        MessageBox.Show("The collateral of a loan cannot be less than 5% of the loan balance.");
                        return false;
                    }
                }

            }

            if (string.IsNullOrEmpty(textBoxLien.Text.ToString()))
            {
                lienAmt = 0;
            }
            else
            {
                lienAmt = int.Parse(textBoxLien.Text.ToString());

                // Check if the lien amount is greater than the loan balance
                if (lienAmt >= loanBalance)
                {

                    MessageBox.Show("The lien amount cannot be greater than the loan balance.");
                    return false;
                }
                // Check if the lien amount is greater than 75% of the loan balance
                else if (!(valid.lienRestrict(loanBalance, lienAmt)))
                {

                    MessageBox.Show("The lien amount cannot be greater than 75% of the loan balance.");
                    return false;
                }
            }

            if (checkBoxCoSign.Checked)
            {
                cosigned = true;
            }
            else
            {
                cosigned = false;
            }

            // Check if the collateral amount is greater than the loan balance
            if (collateralAmt > loanBalance)
            {
                MessageBox.Show("The collateral of a loan cannot be greater than the loan balance.");
                return false;
            }

            // If good set values and calculate
            return true;
        }

        /*
        * Method Name: setRiskReportValues
        * Description: Calls the Calculation methods that set the values used in the Risk Report
        */
        private void setRiskReportValues()
        {
            // Form Variables
            if (validPers == true && lowIncomeLend == "N")
            {
                Calculation.setCreditScore(creditScore);
                Calculation.setGrossIncome(grossIncome);
                Calculation.setEmployment(employStat);
                Calculation.setMinor(minor);
                Calculation.setInterest(intRate);
                Calculation.setBalance(loanBalance);
                Calculation.setUnsecured(unsecured);
                Calculation.setCollateral(collateralAmt, loanBalance);
                Calculation.setLien(lienAmt, loanBalance);
                Calculation.setCosigned(cosigned);
                Calculation.setDeposit(debitonfile, loanBalance);
                Calculation.setDebt(debtonfile, debitonfile);

                Calculation.setCalcType("full");
            }
            else if (validPers == true && lowIncomeLend == "Y")
            {
                Calculation.setMinor(minor);
                Calculation.setInterest(intRate);
                Calculation.setBalance(loanBalance);
                Calculation.setUnsecured(unsecured);
                Calculation.setCollateral(collateralAmt, loanBalance);
                Calculation.setLien(lienAmt, loanBalance);
                Calculation.setCosigned(cosigned);
                Calculation.setDeposit(debitonfile, loanBalance);
                Calculation.setDebt(debtonfile, debitonfile);

                Calculation.setCalcType("fullLIL");
            }
            else if (validPers == false && lowIncomeLend == "N")
            {
                Calculation.setCreditScore(creditScore);
                Calculation.setGrossIncome(grossIncome);
                Calculation.setEmployment(employStat);
                Calculation.setMinor(minor);
                Calculation.setInterest(intRate);
                Calculation.setBalance(loanBalance);
                Calculation.setUnsecured(unsecured);
                Calculation.setCollateral(collateralAmt, loanBalance);
                Calculation.setLien(lienAmt, loanBalance);
                Calculation.setCosigned(cosigned);

                Calculation.setCalcType("part");
            }
            else
            {
                Calculation.setMinor(minor);
                Calculation.setInterest(intRate);
                Calculation.setBalance(loanBalance);
                Calculation.setUnsecured(unsecured);
                Calculation.setCollateral(collateralAmt, loanBalance);
                Calculation.setLien(lienAmt, loanBalance);
                Calculation.setCosigned(cosigned);

                Calculation.setCalcType("partLIL");
            }


        }

        /*
        * Method Name: checkBoxUnsecure_CheckedChanged
        * Description: Disables the collateral control if the loan is unsecured; will contain logic to check if unsecured loans are allowed
        */
        private void checkBoxUnsecure_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxUnsecure.Checked)
            {
                textBoxCollateral.Clear();
                textBoxCollateral.Enabled = false;
            }
            else
            {
                textBoxCollateral.Enabled = true;
            }
        }

    }
}
