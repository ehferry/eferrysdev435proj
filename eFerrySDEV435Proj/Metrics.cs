﻿/* Name: Emma H Ferry
 * Project: SDEV 435 - Final
 * File Description: This is the cs file connected to the main screen of the loan risk calculator app. 
 * It contains three buttons that the user can use to navigate or exit the app. 
 */

// Includes
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eFerrySDEV435Proj
{

    public partial class Metrics : Form
    {
        // Form referenced classes
        Configuration configure = new Configuration();
        Validate valid = new Validate();

        public Metrics()
        {
            InitializeComponent();
            setConfigControls();
        }

        /*
       * Method Name: buttonCancelClose_Click
       * Description: Checks if teh user wishes to exit without saving any changes
       */
        private void buttonCancelClose_Click(object sender, EventArgs e)
        {
            // Check if user truly wishes to close the form without saving any changes
            DialogResult closeCheck = MessageBox.Show("Are you sure you wish to exit? No changes will be saved.", "Exit", MessageBoxButtons.OKCancel);

            if (closeCheck == DialogResult.OK)
            {
                //This will close the metric form
                this.Close();
            }
        }

        /*
       * Method Name: buttonProcess_Click
       * Description: Checks the values input by the user and runs the calculations for the Risk Report screen if they are valid
       */
        private void buttonProcess_Click(object sender, EventArgs e)
        {
            // Check if user truly wishes to close the form without saving any changes
            DialogResult closeCheck = MessageBox.Show("Are you sure you wish to save these settings?", "Process", MessageBoxButtons.OKCancel);

            if (closeCheck == DialogResult.OK)
            {
                // Validate user input
                if(valid.isInteger(textBoxLowBal.Text.ToString()))
                {
                    if(Convert.ToInt32(textBoxLowBal.Text.ToString()) > 0)
                    {
                        if (valid.isInteger(textBoxHighBal.Text.ToString()))
                        {
                            if (Convert.ToInt32(textBoxHighBal.Text.ToString()) > 0)
                            {
                                if (Convert.ToInt32(textBoxLowBal.Text.ToString()) <= Convert.ToInt32(textBoxHighBal.Text.ToString()))
                                {
                                    if (valid.isInteger(textBoxCSMin.Text.ToString()))
                                    {

                                        if (valid.isCreditScore(Convert.ToInt32(textBoxCSMin.Text.ToString()), 300))
                                        {
                                            //Process the new metric settings
                                            setNewConfig();
                                            //This will close the metric form
                                            this.Close();
                                        }
                                        else
                                        {
                                            MessageBox.Show("The minimum credit score must be a valid score between 300 and 850");
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("The minimum credit score must have an integer value to continue.");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("The highest allowed balance cannot be less than the lowest allowed balance.");
                                }
                            }
                            else
                            {
                                MessageBox.Show("The highest allowed balance cannot be less than or equal to zero.");
                            }
                        }
                        else
                        {
                            MessageBox.Show("The highest allowed balance must have an integer value to continue.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The lowest allowed balance cannot be less than or equal to zero.");
                    }
                    
                }
                else
                {
                        MessageBox.Show("The lowest allowed balance must have an integer value to continue.");
                }

            }
        }

        /*
       * Method Name: setConfigControls
       * Description: Sets the displayed metric values based on the values saved in the App.config file
       */
        private void setConfigControls()
        {
            // Set controls to reflect the current metrics
            textBoxLowBal.Text = configure.getConfig("LBAL");
            textBoxHighBal.Text = configure.getConfig("HBAL");
            textBoxCSMin.Text = configure.getConfig("CRED");

            if (configure.getConfig("UNS") == "Y")
            {
                checkBoxUnsecureYN.Checked = true;
            }
            else
            {
                checkBoxUnsecureYN.Checked = false;
            }

            if (configure.getConfig("LIL") == "Y")
            {
                checkBoxLowIncomeYN.Checked = true;
            }
            else
            {
                checkBoxLowIncomeYN.Checked = false;
            }
            
        }

        /*
       * Method Name: setNewConfig
       * Description: Checks the values input by the user and runs the calculations for the Risk Report screen if they are valid
       */
        private void setNewConfig()
        {
            // Set App.config values using the method found in the Configuration class
            configure.setConfig("LBAL", textBoxLowBal.Text.ToString());
            configure.setConfig("HBAL", textBoxHighBal.Text.ToString());
            configure.setConfig("CRED", textBoxCSMin.Text.ToString());

            if (checkBoxUnsecureYN.Checked == true)
            {
                configure.setConfig("UNS", "Y");
            }
            else
            {
                configure.setConfig("UNS", "N");
            }

            if (checkBoxLowIncomeYN.Checked == true)
            {
                configure.setConfig("LIL", "Y");
            }
            else
            {
                configure.setConfig("LIL", "N");
            }
        }
    }
}
