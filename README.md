------------
-- README --
------------
Version 1.00

The purpose of this app is to create a small 'proof of concept' loan risk calculator.
This application consists of four screens which allow a user to calculate a risk report based on user inputs. 
These inputs are checked against parameters also set by the user, and run through basic data snaitation before being passed to the calculation logic and ultimately displayed in a report. 
Any number of loans with any combination of parameters may be run before closing the app.
Version 1.00 of this application was created for SDEV435 and the nature of this project means the app is not designed to be run against databases other than the one used for development.